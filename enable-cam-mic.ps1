# Find the PNPDeviceID 
# Microphone	Get-WmiObject Win32_PnPEntity | where -Property PNPClass -eq audioendpoint
# Camera		Get-WmiObject Win32_PnPEntity | where -Property PNPClass -eq camera
# Check the device names and copy the PNPDeviceIDs of ones you want to enable/disable

# Self-elevate the script if required
# http://www.expta.com/2017/03/how-to-self-elevate-powershell-script.html
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
 if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
  $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
  Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
  Exit
 }
}

$cam = Get-WmiObject Win32_PnPEntity | Where-Object -Property PNPDeviceID -eq "<Camera PNPDeviceID>"
$mic = Get-WmiObject Win32_PnPEntity | Where-Object -Property PNPDeviceID -eq "<Microphone PNPDeviceID>"

$cam.Enable()
$mic.Enable()