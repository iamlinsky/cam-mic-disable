# Use
Identify the devices you want to control, copy their `PNPDeviceID` into the script where indicated

Run either script to enable or disable the camera and microphone


## Find the PNPDeviceID 
Run the following in PowerShell

Microphone: `Get-WmiObject Win32_PnPEntity | where -Property PNPClass -eq audioendpoint`

Camera:     `Get-WmiObject Win32_PnPEntity | where -Property PNPClass -eq camera`

Check the device names and copy the `PNPDeviceID` of ones you want to enable/disable